package com.alikhani.test;

import java.util.List;

public interface StudentService {

    Student insert(Student student);
    Student update(Student  student);
    List<Student> getAllStudent();
    Student getById(Long id);
    void delete(Long id);
}
