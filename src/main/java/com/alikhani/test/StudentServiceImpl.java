package com.alikhani.test;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class StudentServiceImpl implements StudentService{
    private final StudentRepository studentRepository;
    @Override
    public Student insert(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student update(Student student) {
        Student findStudent = getById(student.getId());
        findStudent.setFirstName(student.getFirstName());
        findStudent.setLastName(student.getLastName());
        findStudent.setAge(student.getAge());
        findStudent.setBirth_date(student.getBirth_date());
        findStudent.setAge(student.getAge());
        findStudent.setFatherName(student.getFatherName());
        findStudent.setMotherName(student.getMotherName());
        findStudent.setNationalCode(student.getNationalCode());
        findStudent.setProfileImage(student.getProfileImage());

        Student newStudent = studentRepository.save(findStudent);
        return newStudent;
    }

    @Override
    public List<Student> getAllStudent() {
        List<Student> studentList = (List<Student>) studentRepository.findAll();
        return studentList;
    }

    @Override
    public Student getById(Long id) {
        Optional<Student> optionalStudent = studentRepository.findById(id);

        if (!optionalStudent.isPresent()){
            throw new RuntimeException("Not found");
        }
        return optionalStudent.get();
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }
}
