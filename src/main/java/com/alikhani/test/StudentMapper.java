package com.alikhani.test;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Date;
import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {
    @Mapping( target = "birthDate" , qualifiedByName = "birthDateTimestampToDates" )
    Student toStudent(StudentDto studentDto);
    @Mapping( target = "birthDate" , qualifiedByName = "birthDateToTimestamps" )

    StudentDto toStudentDto(Student student);

    List<StudentDto> toStudentDtos(List<Student> students);

    List<Student> toStudents(List<StudentDto> studentDtos);

    @Named("birthDateTimestampToDates")
    default Date toDate(Long timestamp) {
        if (timestamp != null)
            return new Date(timestamp);
        else
            return null;
    }

    @Named("birthDateToTimestamps")
    default Long toTimestamps(Date date) {
        if (date != null)
            return date.getTime();
        else
            return null;
    }
}
