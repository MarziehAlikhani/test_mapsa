package com.alikhani.test;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/v1/student")
public class StudentController {
    private final StudentService studentService;

    private final StudentMapper studentMapper;

    @PostMapping("/save")
    public ResponseEntity insert(@RequestBody StudentDto studentDto){
        Student student = studentMapper.toStudent(studentDto);
        studentService.insert(student);
        return   ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/update")
    public ResponseEntity update(@RequestBody StudentDto studentDto){
        Student student = studentMapper.toStudent(studentDto);
        studentService.update(student);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/get")
    public ResponseEntity<List<StudentDto>> getAll(){
        List<Student> studentList = studentService.getAllStudent();
        List<StudentDto> studentDtoList = studentMapper.toStudentDtos(studentList);
        return ResponseEntity.ok(studentDtoList);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<StudentDto> getById(@PathVariable Long id){
        Student student = studentService.getById(id);
        return ResponseEntity.ok(studentMapper.toStudentDto(student));
    }
}
