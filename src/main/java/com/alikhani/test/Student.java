package com.alikhani.test;

import com.alikhani.test.common.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.awt.*;
import java.util.Date;

@Data
@Entity
@Table(name = "student")
public class Student extends BaseEntity {
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "national_Code")
    private String nationalCode;

    @Column(name = "profile_image")
    private String profileImage;

    @Column(name = "age")
    private Integer age;

    @Temporal(TemporalType.TIMESTAMP)
    private Date birth_date;

    @Transient
    @Column(name = "mother_name")
    private String motherName;

    @Transient
    @Column(name = "father_name")
    private String fatherName;

}
