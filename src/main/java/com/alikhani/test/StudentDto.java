package com.alikhani.test;

import com.alikhani.test.common.BaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class StudentDto extends BaseDto {
    @ApiModelProperty(required = true, hidden = false)
    private String firstName;

    @ApiModelProperty(required = true, hidden = false)
    private String lastName;

    @ApiModelProperty(required = true, hidden = false)
    private String nationalCode;

    @ApiModelProperty(required = false, hidden = true)
    private String profileImage;

    @ApiModelProperty(required = true, hidden = false)
    private Integer age;

    @ApiModelProperty(required = true, hidden = false)
    private Long birthDate;

    @ApiModelProperty(required = true, hidden = false)
    private String motherName;

    @ApiModelProperty(required = true, hidden = false)
    private String fatherName;

}
